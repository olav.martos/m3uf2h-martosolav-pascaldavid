﻿using System;

namespace TemperatureConvertor
{
    class TemperatureConvertor
    {
        static void Main(string[] args)
        {
            double gradesConverted = 0.0;

            //Console.WriteLine("\nIntrodueix temperatura en Celisus");
            //grades = double.Parse(Console.ReadLine());
            //gradesConverted = (grades * 1.8) + 32;
            //Console.WriteLine("\nLa temperatura en Farenheit es");
            //Console.WriteLine(gradesConverted);
            //Console.WriteLine("\nLa temperatura en Kelvin es");
            //gradesConverted = grades + 273.15;
            //Console.WriteLine(gradesConverted);
            Console.WriteLine("Introduce grados mesura i conversió");
            string grades  =Console.ReadLine().ToLower();
            string[] temperatura = grades.Split(' ');
            if (temperatura[1] == "celsius" && temperatura[2] == "fahrenheit")
            {
                gradesConverted = (Convert.ToDouble(temperatura[0]) * 1.8) + 32;
            }
            else if(temperatura[1] == "celsius" && temperatura[2] == "kelvin") 
            {
                gradesConverted = Convert.ToDouble(temperatura[0]) + 273.15;
            }else if (temperatura[1] == "fahrenheit" && temperatura[2] == "celsius")
            {
                gradesConverted = (Convert.ToDouble(temperatura[0]) -32) / 1.8;
            }
            else if (temperatura[1] == "fahrenheit" && temperatura[2] == "kelvin")
            {
                gradesConverted = (Convert.ToDouble(temperatura[0]) -32) *5/9 + 273.15;
            }
            else if (temperatura[1] == "kelvin" && temperatura[2] == "celsius")
            {
                gradesConverted = Convert.ToDouble(temperatura[0]) - 273.15;
            }
            else if (temperatura[1] == "kelvin" && temperatura[2] == "fahrenheit")
            {
                gradesConverted = (Convert.ToDouble(temperatura[0])-273.15)*9/5+32 ;
            }
            Console.WriteLine(gradesConverted);
        }


    }
}
