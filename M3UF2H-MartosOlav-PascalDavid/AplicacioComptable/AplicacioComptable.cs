﻿using System;
using System.Linq;
namespace AplicacioComptable
{
    class AplicacioComptable
    {
        static void Main(string[] args)
        {
            var menu = new AplicacioComptable();
            Console.WriteLine("1.-Afegir un ingres");
            Console.WriteLine("2.-Afegir una despesa");
            Console.WriteLine("3.-Estadistiques");
            Console.WriteLine("4.-Visualitzacio del compte");
            Console.WriteLine("5.-Impostos");
            Console.WriteLine("Que vols fer: ");
            var option = Console.ReadLine();

            // Ingresos, Despesas, Veces Ingresos, Veces Despesas, Ingreso pequeño, Despesa grande
            double[] array = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            do
            {
                switch (option)
                {
                    case "1":
                        menu.AñadirIngreso(array);
                        break;
                    case "2":
                        menu.AñadirDespesa(array);
                        break;
                    case "3":
                        menu.Estadistiques(array);
                        break;
                    case "4":
                        //menu.VisualitzacioComptes(array);
                        break;
                    case "5":
                        menu.Impostos(array);
                        break;
                }
                Console.WriteLine("Que vols fer: ");
                option = Console.ReadLine();
            } while (option != "END");

        }

        void AñadirIngreso(double[] array)
        {
            Console.WriteLine("Dame un ingreso");
            double valor = Convert.ToDouble(Console.ReadLine());
            array[0] += valor;
            if (array[2] == 0.0)
            {
                array[2]++;
                array[4] = valor;
            }
            else
            {
                if (array[4] > valor)
                {
                    array[4] = valor;
                }
                array[2]++;
            }
            
            return;
        }

        void AñadirDespesa(double[] array)
        {
            Console.WriteLine("Añade una despesa");
            double valor = Convert.ToDouble(Console.ReadLine());
            if (valor < 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Comptabilitat al descobert, amb una descompensacio de: {valor}");
            }
            else
            {
                array[1] += valor;
                if (valor > array[5])
                {
                    array[5] = valor;
                }
                array[3]++ ;
            }
        }

        void Estadistiques(double[] array)
        {
            Console.WriteLine("1.-Mostra la despesa mes gran");
            Console.WriteLine("2.-Ingreso mas pequeño");
            Console.WriteLine("3.-Media ingresos");
            Console.WriteLine("4.-Media despesas");
            Console.WriteLine("Que vols fer: ");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    Console.WriteLine(array[5]);
                    break;
                case "2":
                    Console.WriteLine(array[4]);
                    break;
                case "3":
                    Console.WriteLine(array[0]/array[2]);
                    break;
                case "4":
                    Console.WriteLine(array[1] / array[3]);
                    break;
            }
        }

        void  VisualitzacioComptes(double[] array)
        {
            Console.WriteLine("1.-Mostra total despeses");
            Console.WriteLine("2.-Mostrar total de ingresos");
            Console.WriteLine("3.-Total actual");
            Console.WriteLine("Que vols fer: ");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    Console.WriteLine(array[1]);
                    break;
                case "2":
                    Console.WriteLine(array[0]);
                    break;
                case "3":
                    Console.WriteLine(array[0]-array[1]); 
                    break;
            
            }
            
        }

        void Impostos(double[] ingresos)
        {
            Console.WriteLine("1.-Mostra total IVA");
            Console.WriteLine("2.-Mostrar total de IRPF");
            Console.WriteLine("3.-Ingresos netos");
            Console.WriteLine("Que vols fer: ");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    double valor = IVA(ingresos);
                    Console.WriteLine(valor);
                    break;
                case "2":
                    valor = IRPF(ingresos);
                    Console.WriteLine(valor);
                    break;
                case "3":
                    valor = Netos(ingresos);
                    Console.WriteLine(valor);
                    break;

            }

            double IVA(double[] ingresos)
            {
                double valor = ingresos[0];
                return valor * 0.21;
            }

            double IRPF(double[] ingresos)
            {
                double iva = IVA(ingresos);
                return iva * 0.15;
            }

            double Netos(double[] ingresos)
            {
                double iva = IVA(ingresos);
                double irpf = IRPF(ingresos);
                return (ingresos.Sum() - iva - irpf);
            }
        }
    }
}
