﻿using System;

namespace CodigoEspaguetti
{
    class CodigoEspaguetti
    {
        public static void Main()
        {
            var menu = new CodigoEspaguetti();
            Console.WriteLine("\n-------------------- Calculator -----------------------\n");
            Console.WriteLine("Bienvenido al menú\n");
            Console.WriteLine("-------------------------------------\n");
            string command;
            int num1 = 0;
            int num2 = 0;
            Console.WriteLine("Por favor, introduzca el comando : ");
            command = Console.ReadLine();
            Console.WriteLine("-------------------------------------\n");
            string[] commandSplitted = command.Split(" ");

            if (menu.Error(commandSplitted) == true)
            {
                string operation = commandSplitted[0];
                num1 = int.Parse(commandSplitted[1]);
                num2 = int.Parse(commandSplitted[2]);
                int resultado = 0;

                int result = menu.Resultados(operation, num1, num2);

                Console.WriteLine("El resultado de la operación es : " + result.ToString() + "\n");
                Console.WriteLine("-------------------- Calculator -----------------------\n");
                Console.WriteLine("Presione tecla r para reiniciar\n");
                string reiniciar = Console.ReadLine();

                if (reiniciar == "r")
                {
                    Console.Clear();
                    Main();
                }
                Console.ReadKey();
            }
        }

        bool Error(string[] command)
        {
            if (command.Length < 2)
            {
                Console.WriteLine("------------ ERROR -----------------\n");
                Console.WriteLine("------------ Debe añadir dos valores a operar -----------------\n");
                return false;
            }
            return true;
        }

        int Resultados(string operation, int num1, int num2)
        {
            int resultado = 0;
            switch (operation)
            {
                case "suma":
                    resultado = num1 + num2;
                    return resultado;
                case "resta":
                    resultado = num1 - num2;
                    return resultado;
                case "multiplica":
                    resultado = num1* num2;
                    return resultado;
                case "divide":
                    resultado = num1 / num2;
                    return resultado;
                default:
                    return resultado;
            }
}
    }
}
